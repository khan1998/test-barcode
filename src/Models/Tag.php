<?php

// namespace App\Models;
namespace Smertgenie\Smertinventory\Warehouse\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class Tag extends Model
{
    use HasFactory;
    protected $guarded   = ['id'];

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}

