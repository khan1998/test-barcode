<?php

namespace Smertgenie\Smertinventory\Warehouse\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    use HasFactory;
    protected $guarded    = ['id'];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

}
