<?php

namespace Smertgenie\Smertinventory\Warehouse\Models;

use App\Traits\Historiable;

use App\Traits\UserTrackable;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{   
    // use Historiable, UserTrackable, NodeTrait;
    protected $connection = 'inventory';
    protected $table      = 'warehouses';
    protected $guarded    = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function stores()
    {
        return $this->hasMany(\Smertgenie\Smertinventory\Warehouse\Models\Store::class);
    }

    ##ELOQUENTRELATIONSHIPMODEL##
}