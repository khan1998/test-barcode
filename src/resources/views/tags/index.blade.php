@extends('layouts.master')

@section('content')
<x-utilities.card>
    <x-slot name="heading">
        {{ __('Store') }}
    </x-slot>
    <x-slot name="body">
        <ul class="nav nav-sidebar w-50" data-nav-type="accordion">
            <li class="nav-item nav-item-submenu ">
                <a href="#" class="nav-link text-dark">
                    <i class="icon-tree5"></i>
                    <span id="topWarehouses"> {{ __('Warehouses') }}</span>
                </a>
                
            </li>
        </ul>
        <x-utilities.link-create class="float-right my-2" href="{{route('tags.create')}}">+ {{ __('Add New') }}
        </x-utilities.link-create>
        <x-alerts.message :message="session('success')" />
        <x-utilities.table-basic id="storeDatatable" class="display responsive nowrap" style="width:100%">
            <x-utilities.table-basic-head class="bg-info">
                <tr>
                    <th>{{ __('SL') }}</th>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Actions' )}}</th>
                </tr>
            </x-utilities.table-basic-head>
            <x-utilities.table-basic-body>
                @foreach ($tags as $tag)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $tag->title }}</td>
                    <td class="d-flex justify-content-center">
                        <x-utilities.link-show href="{{route('tags.show', $tag->uuid)}}">{{ __('Show') }}
                        </x-utilities.link-show>

                        <x-utilities.link-edit href="{{route('tags.edit', $tag->uuid)}}">{{ __('Edit') }}
                        </x-utilities.link-edit>

                        <form style="display:inline" action="{{route('tags.destroy', $tag->uuid)}}"
                            method="post">
                            @csrf
                            @method('delete')
                            <x-forms.button class="btn btn-danger"
                                onClick="return confirm('Are you sure want to delete ?')" type="submit">{{ __('Delete')
                                }}</x-forms.button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </x-utilities.table-basic-body>
        </x-utilities.table-basic>
        {{ $tags->links('pagination::bootstrap-5') }}
    </x-slot>
</x-utilities.card>
@endsection