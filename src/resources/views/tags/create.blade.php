@extends('layouts.master')

@section('content')
<x-utilities.card>
	<x-slot name="heading">
		{{ __('Warehouse') }}
		<x-utilities.link-list href="{{route('tags.index')}}">{{ __('List') }}</x-utilities.link-list>
	</x-slot>
	<x-slot name="body">
	<x-alerts.errors :errors="$errors" />
		<form action="{{ route('tags.store') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<!-- <div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />
				<select name="parent_id" class="form-control">
					@foreach ($tags as $tag)
						<option value="{{ $tag->id }}">{{ $tag->title }}</option>
					@endforeach
				</select>
			</div> -->
			<div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />
				<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')"
					placeholder="" />
				<x-forms.error name="title" />
			</div>
			<div class="mb-3">
				<x-forms.label class="form-label" for="imageInput" :value="__('Image')" />
				<x-forms.input type="file" class="form-control-file" id="imageInput" name="image" :value="old('image')"
					placeholder="" />
			</div>

			<x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button>
		</form>
	</x-slot>
</x-utilities.card>
@endsection