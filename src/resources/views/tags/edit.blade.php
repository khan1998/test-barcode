@extends('layouts.master')

@section('content')

<x-utilities.card>
	<x-slot name="heading">
		{{ __('Warehouse') }}
		<x-utilities.link-list href="{{route('tags.index')}}">{{ __('List') }}</x-utilities.link-list>
	</x-slot>
	<x-slot name="body">
		
		<form action="{{ route('tags.update', $tag->uuid) }}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('PATCH')
			{{--relationalFields--}}
			<!-- title -->
			<div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('')" />

				<x-forms.input type="text" class="form-control" id="titleInput" name="title"
					:value="old('title', $tag->title)" placeholder="" />

				
			</div>
			<!-- description -->
				<div class="mb-3">
				<x-forms.label class="form-label" for="imageInput" :value="__('Image')" />
				<x-forms.input type="file" class="form-control-file" id="imageInput" name="image" :value="old('image')"
					placeholder="" />
			</div>

			<x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button>
		</form>
	</x-slot>
</x-utilities.card>
@endsection