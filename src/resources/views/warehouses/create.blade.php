@extends('layouts.master')

@section('content')
<x-utilities.card>
	<x-slot name="heading">
		{{ __('Warehouse') }}
		<x-utilities.link-list href="{{route('warehouses.index')}}">{{ __('List') }}</x-utilities.link-list>
	</x-slot>
	<x-slot name="body">
		<x-alerts.errors :errors="$errors" />
		<form action="{{ route('warehouses.store') }}" method="POST">
			@csrf
			<div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />
				<select name="parent_id" class="form-control">
					@foreach ($warehouses as $warehouse)
						<option value="{{ $warehouse->id }}">{{ $warehouse->title }}</option>
					@endforeach
				</select>
				<x-forms.error name="title" />
			</div>
			<div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />
				<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')"
					placeholder="" />
				<x-forms.error name="title" />
			</div>
			<!-- description -->
			<div class="mb-3">
				<x-forms.label class="form-label" for="descriptionInput" :value="__('Description')" />

				<x-forms.textarea class="form-control" name="description" id="descriptionInput"
					:value="old('description')" />

				<x-forms.error name="description" />
			</div>

			<x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button>
		</form>
	</x-slot>
</x-utilities.card>
@endsection