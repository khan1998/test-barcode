@extends('layouts.master')

@section('content')

<x-utilities.card>
    <x-slot name="heading">
        {{ __('Warehouse') }}
        <x-utilities.link-list href="{{route('warehouses.index')}}">{{ __('List') }}</x-utilities.link-list>
    </x-slot>
    <x-slot name="body">
        <p><b>{{ __('') }} : </b> {{ $warehouse->title }}</p>
        <p><b>{{ __('') }} : </b> {{ $warehouse->description }}</p>

        {{-- @if(count($warehouse->stores))
        <h3>{{ __('Store') }}</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>{{ __('Title') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($warehouse->stores as $store)
                <tr>
                    <td>{{ $store->store_title }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif --}}
        {{--othersInfo--}}
    </x-slot>
</x-utilities.card>

@endsection