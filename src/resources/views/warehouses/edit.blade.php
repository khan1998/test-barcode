@extends('layouts.master')

@section('content')

<x-utilities.card>
	<x-slot name="heading">
		{{ __('Warehouse') }}
		<x-utilities.link-list href="{{route('warehouses.index')}}">{{ __('List') }}</x-utilities.link-list>
	</x-slot>
	<x-slot name="body">
		<x-alerts.errors :errors="$errors" />
		<form action="{{ route('warehouses.update', $warehouse->uuid) }}" method="POST">
			@csrf
			@method('PATCH')
			{{--relationalFields--}}
			<!-- title -->
			<div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('')" />

				<x-forms.input type="text" class="form-control" id="titleInput" name="title"
					:value="old('title', $warehouse->title)" placeholder="" />

				<x-forms.error name="title" />
			</div>
			<!-- description -->
			<div class="mb-3">
				<x-forms.label class="form-label" for="descriptionInput" :value="__('')" />

				<x-forms.textarea class="form-control" name="description" id="descriptionInput"
					:value="old('description', $warehouse->description)" />

				<x-forms.error name="description" />
			</div>
			<x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button>
		</form>
	</x-slot>
</x-utilities.card>
@endsection