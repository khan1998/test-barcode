@extends('layouts.master')

@section('content')

<x-utilities.card>
    <x-slot name="heading">
        {{ __('Warehouse') }}
        <x-utilities.link-list href="{{route('configurations.index')}}">{{ __('List') }}</x-utilities.link-list>
    </x-slot>
    <x-slot name="body">
        <p><b>{{ __('') }} : </b> {{ $configuration->title }}</p>
        <p><b>{{ __('') }} : </b> {{ $configuration->uuid }}</p>

        {{-- @if(count($tag->stores))
        <h3>{{ __('Store') }}</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>{{ __('Title') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tag->stores as $store)
                <tr>
                    <td>{{ $store->store_title }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif --}}
        {{--othersInfo--}}
    </x-slot>
</x-utilities.card>

@endsection