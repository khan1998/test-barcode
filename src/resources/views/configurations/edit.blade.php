@extends('layouts.master')

@section('content')

<x-utilities.card>
	<x-slot name="heading">
		{{ __('Warehouse') }}
		<x-utilities.link-list href="{{route('configurations.index')}}">{{ __('List') }}</x-utilities.link-list>
	</x-slot>
	<x-slot name="body">
		
		<form action="{{ route('configurations.update', $configuration->uuid) }}" method="POST">
			@csrf
			@method('PATCH')
			{{--relationalFields--}}
			<!-- title -->
			<div class="mb-3">
				<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />
				<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title', $configuration->title)"
					placeholder="" />
			</div>
			<div class="mb-3">
				<x-forms.label class="form-label" for="aliasInput" :value="__('Alias')" />
				<x-forms.input type="text" class="form-control" id="titleInput" name="alias" :value="old('alias',  $configuration->alias)"
					placeholder="" />
			</div>
			<div class="mb-3">
				<x-forms.label class="form-label" for="operationInput" :value="__('Operation')" />
				<x-forms.input type="text" class="form-control" id="titleInput" name="operation" :value="old('operation',  $configuration->operation)"
					placeholder="" />
			</div>
			<div class="mb-3 form-check">
				
				<x-forms.input type="checkbox" class="form-check-input" id="is_activeInput" name="is_active" value="1"
					placeholder="" />
				<x-forms.label class="form-check-label" for="is_activeInput" :value="__('Is_Active')" />
			</div>
			<br><br>
			<x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button>
		</form>
	</x-slot>
</x-utilities.card>
@endsection