<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_locations', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('bin_card_id');
            $table->integer('bin_transaction_id');
            $table->integer('item_id');
            $table->integer('location_id');
            $table->string('location');
            $table->string('located_items');
            $table->integer('located_qty');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bin_locations');
    }
};
