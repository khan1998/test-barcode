<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('srcs', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('store_model');
            $table->integer('store_id');
            $table->integer('item_id');
            $table->string('title');
            $table->string('description');
            $table->string('currency');
            $table->integer('price');
            $table->string('source_of_supply');
            $table->string('uom');
            $table->integer('total_received_items');
            $table->integer('total_received_qty');
            $table->integer('total_delivered_items');
            $table->integer('total_delivered_qty');
            $table->integer('stock_items');
            $table->integer('stock_qty');
            $table->timestamp('first_received_at');
            $table->timestamp('last_received_at');
            $table->timestamp('first_delivered_at');
            $table->timestamp('last_delivered_at');
            $table->integer('min_receive');
            $table->integer('max_receive');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srcs');
    }
};
