<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('quotation_id');
            $table->integer('quotation_no');
            $table->integer('seles_order_no');
            $table->string('seles_order_type');
            $table->integer('customer_id');
            $table->string('customer_name');
            $table->integer('item_qty');
            $table->integer('total_item_price');
            $table->integer('sell_discount_amount');
            $table->integer('sub_total');
            $table->integer('vat_tax');
            $table->integer('grand_total');
            $table->string('doc_attached');
            $table->string('note');
            $table->timestamp('approved_at');
            $table->integer('approved_by');
            $table->timestamp('unapproved_at');
            $table->integer('unapproved_by');
            $table->string('unapproved_comment');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders');
    }
};
