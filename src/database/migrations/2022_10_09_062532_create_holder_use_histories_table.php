<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holder_use_histories', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('inventory_holder_id');
            $table->string('used_items');
            $table->integer('used_qty');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holder_use_histories');
    }
};
