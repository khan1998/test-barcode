<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('store_model');
            $table->integer('request_from_store_id');
            $table->integer('request_to_store_id');
            $table->integer('item_id');
            $table->string('uom');
            $table->integer('loan_reference_no');
            $table->timestamp('request_iteams');
            $table->timestamp('request_qty');
            $table->integer('delivered_items');
            $table->integer('delivered_qty');
            $table->integer('received_items');
            $table->integer('received_qty');
            $table->integer('return_items');
            $table->integer('return_qty');
            $table->integer('pay_off_debt_items');
            $table->integer('pay_off_debt_qty');
            $table->timestamp('approved_at');
            $table->integer('approved_by');
            $table->timestamp('unapproved_at');
            $table->integer('unapproved_by');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
    }
};
