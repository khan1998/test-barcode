<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_requests', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('return_reference_no');
            $table->integer('item_id');
            $table->string('return_reason');
            $table->string('request_items');
            $table->integer('request_qty');
            $table->string('deliverd_items');
            $table->integer('deliverd_qty');
            $table->timestamp('approved_at');
            $table->integer('approved_by');
            $table->timestamp('unapproved_at');
            $table->integer('unapproved_by');
            $table->string('unapproved_comment');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_requests');
    }
};
