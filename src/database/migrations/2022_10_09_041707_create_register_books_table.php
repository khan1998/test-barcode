<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_books', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('store_model');
            $table->integer('store_id');
            $table->string('title');
            $table->string('description');
            $table->timestamp('financial_year');
            $table->integer('open_stock_items');
            $table->integer('open_stock_qty');
            $table->integer('total_received_items');
            $table->integer('total_received_qty');
            $table->integer('total_delivered_items');
            $table->integer('total_delivered_qty');
            $table->integer('stock_items');
            $table->integer('stock_qty');
            $table->timestamp('first_received_at');
            $table->timestamp('last_received_at');
            $table->timestamp('first_delivered_at');
            $table->timestamp('last_delivered_at');
            $table->integer('min_receive');
            $table->integer('max_receive');
            $table->biginteger('created_by');
            $table->biginteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_books');
    }
};
