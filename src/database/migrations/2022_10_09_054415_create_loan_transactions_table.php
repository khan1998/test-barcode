<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_transactions', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('loan_request_id');
            $table->integer('item_id');
            $table->integer('deliverd_reference_no');
            $table->string('deliverd_items');
            $table->integer('deliverd_qty');
            $table->timestamp('deliverd_at');
            $table->string('deliverd_to');
            $table->integer('received_reference_no');
            $table->string('received_items');
            $table->integer('received_qty');
            $table->timestamp('received_at');
            $table->string('received_from');
            $table->integer('return_reference_no');
            $table->string('return_items');
            $table->integer('return_qty');
            $table->timestamp('return_at');
            $table->integer('pay_off_debt_reference_no');
            $table->string('pay_off_debt_items');
            $table->integer('pay_off_debt_qty');
            $table->timestamp('pay_off_debt_at');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_transactions');
    }
};
