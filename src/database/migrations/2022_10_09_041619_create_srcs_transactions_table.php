<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('srcs_transactions', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('src_id');
            $table->integer('item_id');
            $table->integer('received_reference_no');
            $table->integer('received_items');
            $table->integer('received_qty');
            $table->timestamp('received_at');
            $table->string('received_from');
            $table->integer('delivered_reference_no');
            $table->integer('delivered_items');
            $table->integer('delivered_qty');
            $table->timestamp('delivered_at');
            $table->string('delivered_to');
            $table->integer('stock_items');
            $table->integer('stock_qty');
            $table->string('comment');
            $table->biginteger('created_by');
            $table->biginteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srcs_transactions');
    }
};
