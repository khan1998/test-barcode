<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitions', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('store_id');
            $table->integer('item_id');
            $table->string('uom');
            $table->string('request_items');
            $table->integer('request_qty');
            $table->string('deliverd_items');
            $table->integer('deliverd_qty');
            $table->timestamp('approved_at');
            $table->string('approved_by');
            $table->timestamp('unapproved_at');
            $table->string('unapproved_by');
            $table->string('unapproved_comment');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisitions');
    }
};
