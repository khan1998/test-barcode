<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_item', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('sales_order_id');
            $table->integer('item_id');
            $table->string('item_title');
            $table->string('uom');
            $table->integer('qty');
            $table->integer('challaned_qty');
            $table->integer('unit_price');
            $table->integer('discount_amount');
            $table->integer('discount_percentage');
            $table->integer('sub_total');
            $table->boolean('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_item');
    }
};
