<?php

namespace Smertgenie\Smertinventory\Warehouse;

use Illuminate\Support\ServiceProvider;

class SmertinventoryWarehouseServiceProvider extends ServiceProvider
{
    public function boot()
    {
    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    $this->loadViewsFrom(__DIR__ . '/resources/views', 'smertinventory');

    $this->commands([
        ##InstallationCommandClass##
    ]);
    
    
// \Illuminate\Support\Facades\Blade::component('forms.select-warehouse', \Pondit\Smertgenie\Smertinventory\App\View\Components\Forms\SelectWarehouse::class);

// \Illuminate\Support\Facades\Blade::component('forms.select-store', \Pondit\Smertgenie\Smertinventory\App\View\Components\Forms\SelectStore::class);
##||ANOTHERCOMPONENT||##
    
    }
    public function register()
    { }
}