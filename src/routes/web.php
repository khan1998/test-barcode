<?php 
use Illuminate\Support\Facades\Route;

use Smertgenie\Smertinventory\Warehouse\Http\Controllers\WarehouseController;
use Smertgenie\Smertinventory\Warehouse\Http\Controllers\TagController;
use Smertgenie\Smertinventory\Warehouse\Http\Controllers\ConfigController;
//use namespace

Route::group(['middleware' => 'web'], function () {
	Route::resource('warehouses', WarehouseController::class);
	Route::get('warehouses-list', [WarehouseController::class, 'getData']);
	Route::resource('tags', TagController::class);
	Route::resource('configurations', ConfigController::class);

//Place your route here
});


