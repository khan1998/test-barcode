<?php

namespace Smertgenie\Smertinventory\Warehouse\Http\Controllers;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Smertgenie\Smertinventory\Warehouse\Models\Warehouse;
use Smertgenie\Smertinventory\Warehouse\Http\Requests\WarehouseRequest;
//use another classes

class WarehouseController extends Controller
{
    // public $menudata = '';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $treeData = $this->getMenu($activeId = null);
        // $warehouses = Warehouse::orderBy('id', 'desc')->paginate(10);
        return view('smertinventory::warehouses.index', compact('warehouses'));
    }

    // public function getMenu($activeId = null)
    // {
    //     $menu = '';
    //     $nodes = Warehouse::orderBy('id', 'asc')->get()->toTree();
    //     if(count($nodes)){
    //         $menu = $this->_printitem($nodes, $activeId);
    //     }
    //     return $menu;
    // }

    // private function _printitem($items,$activeId,$submenu_class = '',$link_class = '')
    // {
    //     $this->menudata .='<ul class="nav nav-group-sub">'.PHP_EOL;
    //     foreach($items as $item){
    //         if( count($item->children) > 0 ){
    //             $submenu_class = 'nav-item-submenu';
    //             $link_class = 'legitRipple text-dark';
    //         }
    //         if ($item->id === 11) {
    //             echo $activeId;
    //             $active = 'active';
    //         }else{
    //             $active = '';
    //         }

    //         $this->menudata .='<li class="nav-item '.$submenu_class.' ">'.PHP_EOL;

    //         $this->menudata .='<a href="#" class="nav-link text-dark'.$link_class.''. $active.' "><i class="icon-book"></i>'.$item->title.'</a>'.PHP_EOL;


    //         if( count($item->children) > 0 ){
    //             $child_items = $item->children;
    //             $submenu_class = '';
    //             $link_class = '';
    //             $this->_printitem($child_items,$submenu_class,$link_class);
    //         }

    //         $this->menudata .= '</li>'.PHP_EOL;
    //     }

    //     $this->menudata .='</ul>';

    //     return $this->menudata;
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouses = Warehouse::orderBy('id', 'desc')->get();
        return view('smertinventory::warehouses.create', compact('warehouses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\WarehouseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseRequest $request)
    {
        try {
            $warehouse = Warehouse::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('warehouses.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouse)
    {
        return view('smertinventory::warehouses.show', compact('warehouse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit(Warehouse $warehouse)
    {
        return view('smertinventory::warehouses.edit', compact('warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\WarehouseRequest  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(WarehouseRequest $request, Warehouse $warehouse)
    {
        try {
            $warehouse->update($request->all());
            //handle relationship update
            return redirect()->route('warehouses.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse)
    {
        try {
            $warehouse->delete();

            return redirect()->route('warehouses.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    // public function getData()
    // {
    //     /*Variables*/
    //     $paginatePerPage = \request('rows_per_page') ?? 10;
    //     $query = new Warehouse;
    //     /*Filtering by column*/
    //     if ($filterableColumns = \request('filterable_columns')) {
    //         $columns = explode('|', $filterableColumns);
    //         foreach ($columns as $column) {
    //             $columnArray = explode('=>', $column);
    //             $columnName = $columnArray[0] ?? null;
    //             $columnValue = $columnArray[1] ?? null;
                
    //             if ($columnName && $columnValue) {
                    
    //                 if($columnName == 'created_at_from'){
    //                     $query = $query->whereDate('created_at', '>=', $columnValue);
    //                     continue;
    //                 }

    //                 if($columnName == 'created_at_to'){
    //                     $query = $query->whereDate('created_at', '<=', $columnValue);
    //                     continue;
    //                 }

    //                 if(substr($columnName, -5) == '_like'){
    //                     $columnName = substr($columnName, 0, -5);
    //                     $query = $query->whereRaw("LOWER(`{$columnName}`) LIKE ? ", "%{$columnValue}%");
    //                     continue;
    //                 }
                    
    //                 $query = $query->where($columnName, $columnValue);

    //             }
                
    //         }
    //     }
    //     /////////////////////////

    //     $query = $query->orderBy('id', 'desc');
    //     $data = $query->paginate($paginatePerPage);
    //     $dataArray = $data->toArray();

    //     return response()->json([
    //         'records' => $data,
    //         'pages' => $this->getPages($dataArray['current_page'], $dataArray['last_page'], $dataArray['total']),
    //         'sl' => !is_null(\request()->page) ? (\request()->page -1) * $paginatePerPage : 0
    //     ]);
    // }

    // private function getPages($currentPage, $lastPage, $totalPages)
    // {
    //     $startPage = ($currentPage < 5)? 1 : $currentPage - 4;
    //     $endPage = 8 + $startPage;
    //     $endPage = ($totalPages < $endPage) ? $totalPages : $endPage;
    //     $diff = $startPage - $endPage + 8;
    //     $startPage -= ($startPage - $diff > 0) ? $diff : 0;
    //     $pages = [];

    //     if ($startPage > 1) {
    //         $pages[] = '...';
    //     }

    //     for ($i=$startPage; $i<=$endPage && $i<=$lastPage; $i++) {
    //         $pages[] = $i;
    //     }

    //     if ($currentPage < $lastPage) {
    //         $pages[] = '...';
    //     }

    //     return $pages;
    // }
//another methods
}
