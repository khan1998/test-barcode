<?php

namespace Smertgenie\Smertinventory\Warehouse\Http\Controllers;

use Image;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Smertgenie\Smertinventory\Warehouse\Models\Tag;
use Smertgenie\Smertinventory\Warehouse\Http\Requests\TagRequest;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $treeData = $this->getMenu($activeId = null);
        $tags = Tag::orderBy('id')->paginate(10);
        return view('smertinventory::tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::orderBy('id')->get();
        return view('smertinventory::tags.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TagRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        try {
            
            // $tags = Tag::create(['uuid'=> Str::uuid()] + $request->all());

            $requestData = $request->all();
            $requestData['image'] = $this->uploadImage($request->file('image'));
            $product = Tag::create(['uuid'=> Str::uuid()] +$requestData);
            
            //handle relationship store
            return redirect()->route('tags.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    public function uploadImage($file = null)
    {
        if (is_null($file)) return $file;
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $destinationPath = storage_path('app/public/tags/');
        $file->move($destinationPath, $fileName);
        return $fileName;     
    }

    public function show(Tag $tag)
    {
        return view('smertinventory::tags.show', compact('tag'));
    }
    public function edit(Tag $tag)
    {
        return view('smertinventory::tags.edit', compact('tag'));
    }

    public function update(TagRequest $request, Tag $tag)
    {
        try {
            // $tag->update($request->all());
            $requestData = $request->all();
            $requestData['image'] = $request->file('image') ? $this->uploadImage($request->file('image')) : $tag->image;
            $tag->update($requestData);
            //handle relationship update
            return redirect()->route('tags.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }
    public function destroy(Tag $tag)
    {
        try {
            $tag->delete();

            return redirect()->route('tags.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

}
