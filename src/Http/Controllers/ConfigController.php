<?php

namespace Smertgenie\Smertinventory\Warehouse\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Smertgenie\Smertinventory\Warehouse\Models\Configuration;
use Smertgenie\Smertinventory\Warehouse\Http\Requests\ConfigRequest;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $treeData = $this->getMenu($activeId = null);
        $configurations = Configuration::orderBy('id')->paginate(10);
        return view('smertinventory::configurations.index', compact('configurations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $configuration = Configuration::orderBy('id')->get();
        return view('smertinventory::configurations.create', compact('configuration'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TagRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConfigRequest $request)
    {
        try {
            
            $configurations = Configuration::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('configurations.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    public function show(Configuration $configuration)
    {
        return view('smertinventory::configurations.show', compact('configuration'));
    }
    public function edit(Configuration $configuration)
    {
        return view('smertinventory::configurations.edit', compact('configuration'));
    }

    public function update(ConfigRequest $request, Configuration $configuration)
    {
        try {
            $configuration->update($request->all());
            //handle relationship update
            return redirect()->route('configurations.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }
    public function destroy(Configuration $configuration)
    {
        try {
            $configuration->delete();

            return redirect()->route('configurations.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

}
